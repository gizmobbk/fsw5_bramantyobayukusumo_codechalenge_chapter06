// * import module
const express = require("express");
const serverError = require("./ServerError");
const morgan = require("morgan");
const methodOverride = require("method-override");
const handlerTidakDitemukan = require("./handlerTidakDitemukan");

// aktifkan module express
const app = express();

// definisikan PORT
const PORT = 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(morgan("dev"));
app.use(methodOverride("_method"));
// menggunakan view engine ejs
app.use(express.static(__dirname + "/public"));

app.set("view engine", "ejs");

const pageRouter = require("./routes/page.router");
const loginRouter = require("./routes/login.router");
const registerRouter = require("./routes/register.router");
const userRouter = require("./routes/users.router");
const historyRouter = require("./routes/history.router");
const biodataRouter = require("./routes/biodata.router");

// middleware router
app.use("/", pageRouter);
app.use("/login", loginRouter);
app.use("/register", registerRouter);
app.use("/user", userRouter);
app.use("/history", historyRouter);
app.use("/biodata", biodataRouter);
// error handling
app.use(serverError);
app.use(handlerTidakDitemukan);

// jalankan server pada port
app.listen(PORT, () => {
    console.log(`Server berjalan pada port ${PORT}`);
});
