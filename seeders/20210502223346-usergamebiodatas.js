'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('UserGameBiodata', [
      {
        name: 'admin admin',
        address: 'admin',
        hobby: 'admin@admin.com',
        user_id: '1',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'bram bram',
        address: '1234',
        hobby: 'admin@admin.com',
        user_id: '2',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('UserGameBiodata', null, {});
  }
};
