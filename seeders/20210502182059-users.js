'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Users', [
      {
        username: 'admin',
        password: 'admin',
        email: 'admin@admin.com',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: 'bram',
        password: '1234',
        email: 'admin@admin.com',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Users', null, {});
  }
};
