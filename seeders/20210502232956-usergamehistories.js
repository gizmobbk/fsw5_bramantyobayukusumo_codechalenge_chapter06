'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('UserGameHistories', [
      {
        user_name: 'bram',
        status: 'win',
        date_played: new Date(),
        user_id: '2',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_name: 'bram',
        status: 'win',
        date_played: new Date(),
        user_id: '2',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_name: 'bram',
        status: 'win',
        date_played: new Date(),
        user_id: '2',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_name: 'bram',
        status: 'lose',
        date_played: new Date(),
        user_id: '2',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_name: 'bram',
        status: 'draw',
        date_played: new Date(),
        user_id: '2',
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('UserGameHistories', null, {});
  }
};
