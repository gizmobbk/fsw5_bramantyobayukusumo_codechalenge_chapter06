const express = require('express');
const router = express.Router();
const db = require('../models/index');

// render history all user
router.get('/', (req, res) => {
    db.UserGameHistory.findAll({ 
        include: 'user' 
    }).then((userHistories) => {
        res.status(200).render('user-history/history',{userHistories});
    }).catch(err => {
        console.log(err)
        res.status(400).json(err);
    });
});

// add database from game
router.post('/',(req, res)=>{
    const {user_name, status, date_played, user_id} = req.body;
    db.UserGameHistory.create({
        user_name, status, date_played, user_id
    }).then(userHistory => {
        res.status(200);
    }).catch(err => {
        res.status(400).json({ message: err.message});
    });
});

// render history per user
router.get('/:id', (req, res) => {
    const id = req.params.id
    db.UserGameHistory.findAll({ 
        where: {
            user_id: id
        },
        include: 'user' 
    }).then((userHistories) => {
        console.log(userHistories);
        res.status(200).render('user-history/history-user',{userHistories});
    }).catch(err => {
        console.log(err)
        res.status(400).json(err);
    });
});

// update history
router.get('/edit/:id', (req, res) => {
    db.UserGameHistory.findByPk(req.params.id)
    .then(userHistory => {
        if(userHistory){
            res.status(200).render('user-history/edit',{
                id: userHistory.id,
                username: userHistory.user_name,
                status: userHistory.status,
            });
        }
        else {
            res.status(400).json({
                message: "id user is not found"
            });
        }
    }).catch(err => {
        res.status(400).json({
            message: err.message
        })
    })
});
router.put('/edit/:id', (req, res) => {
    db.UserGameHistory.update(req.body, {
        where:{
            id:req.params.id
        }
    }).then(userHistory => {
        res.status(201).redirect('/history');
    }).catch(err => {
        res.status(400).json(`Can't update user - ${err.message}`);
    });
});

// delete history
router.delete('/:id', (req, res) => {
    db.UserGameHistory.destroy({
        where:{
            id:req.params.id
        }
    }).then(userHistory => {
        res.status(201).redirect('/history');
    }).catch(err => {
        res.status(400).json(`Can't delete history - ${err.message}`);
    });
});

module.exports = router;