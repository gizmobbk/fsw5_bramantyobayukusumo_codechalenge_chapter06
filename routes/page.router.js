const express = require('express');
const router = express.Router();

// routing index
router.get('/', (req, res)=>{
    res.render('index');
});

// routing game
router.get('/game', (req, res)=>{
    res.render('game');
});

module.exports = router;