const express = require('express');
const router = express.Router();
const db = require('../models/index');

router.get('/dashboard', (req, res) => {
    db.User.findAll({
        order:[
            ['id', 'ASC']
        ]
    }).then((users) => {
        res.status(200).render('user/admin-dashboard', {users});
    }).catch(err => {
        res.status(400).json({message: err.message});
    })
});

// add user
router.get('/add', (req, res) => {
    res.render('user/add');
});
router.post('/add', (req, res) => {
    const {username, email, password} = req.body;
    db.User.create({
        username, email, password
    }).then(user => {
        res.status(200).redirect('/user/dashboard');
    }).catch(err => {
        res.status(400).redirect('/user/add');
    });
});

// update user account
router.get('/edit/:id', (req, res) => {
    db.User.findByPk(req.params.id)
    .then(user => {
        if(user){
            res.status(200).render('user/edit',{
                id: user.id,
                username: user.username,
                password: user.password,
                email: user.email
            });
        }
        else {
            res.status(400).json({
                message: "id user is not found"
            });
        }
    }).catch(err => {
        res.status(400).json({
            message: err.message
        })
    })
});
router.put('/edit/:id', (req, res) => {
    db.User.update(req.body, {
        where:{
            id:req.params.id
        }
    }).then(user => {
        res.status(201).redirect('/user/dashboard');
    }).catch(err => {
        res.status(400).json(`Can't update user - ${err.message}`);
    });
});

// delete user account
router.delete('/dashboard/:id', (req, res) => {
    db.User.destroy({
        where:{
            id:req.params.id
        }
    }).then(user => {
        res.status(201).redirect('/user/dashboard');
    }).catch(err => {
        res.status(400).json(`Can't delete user - ${err.message}`);
    });
});

module.exports = router;