const express = require('express');
const router = express.Router();
const db = require('../models/index');

// get biodata user
router.get('/:id', (req, res) => {
    const id = req.params.id
    db.UserGameBiodata.findOne({ 
        where: {
            user_id: id
        },
        //include: 'user' 
    }).then((userBio) => {
        if (userBio == null || userBio == undefined){
            res.status(200).render('user-biodata/biodata-user',{
                id: id,
                name: "", 
                address: "", 
                hobby: ""
            });
        }
        else {
                res.status(200).render('user-biodata/biodata-user',{
                id: userBio.user_id,
                name: userBio.name, 
                address: userBio.address, 
                hobby: userBio.hobby
            });
        }
    }).catch((err,userBio) => {
        console.log(err)
        console.log(userBio);
        res.status(400).json(userBio);
    });
});

// update biodata user
router.put('/:id', (req, res) => {
    db.UserGameBiodata.update(req.body, {
        where:{
            id:req.params.id
        }
    }).then(userBio => {
        res.status(201).redirect('/user/dashboard');
    }).catch(err => {
        res.status(400).json(`Can't update user - ${err.message}`);
    });
});

module.exports = router;