const express = require('express');
const router = express.Router();
const db = require('../models/index');

router.get('/', (req, res) => {
    res.render('user/user-login');
});

// check login
router.post('/', (req, res) =>{
    const {username, password} = req.body;
    db.User.findOne({
        where: {
            username: username,
            password: password
        },
        attributes: [
            "id",
            "username",
            "password"
        ]
    }).then(user => {
        if (user.username == 'admin') {
            res.redirect('/user/dashboard');
        } else {
            res.render('game', {
                id: user.id,
                username: user.username
            });
        }
    }).catch(err => {
        res.redirect('/register');
    });
});

module.exports = router;