const express = require('express');
const router = express.Router();
const db = require('../models/index');

router.get('/', (req, res) => {
    res.render('user/user-register');
});

router.post('/', (req, res) => {
    const {username, email, password} = req.body;
    db.User.create({
        username, email, password
    }).then(user => {
        res.status(200).redirect('/login');
    }).catch(err => {
        res.status(400).redirect('/register');
    });
});

module.exports = router;