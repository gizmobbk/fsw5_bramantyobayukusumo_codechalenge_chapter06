'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      username: {
        allowNull: false,
        type: Sequelize.STRING,
        validate: { // validasi agar tidak kosong
          notNull: { msg: 'User must have a username'},
          notEmpty: { msg: 'Username must not be empty'}
        }
      },
      password: {
        allowNull: false,
        type: Sequelize.STRING,
        validate: { // validasi agar tidak kosong
          notNull: { msg: 'User must have a password'},
          notEmpty: { msg: 'Password must not be empty'}
        }
      },
      email: {
        allowNull: false,
        type: Sequelize.STRING,
        validate: { // validasi agar tidak kosong
          notNull: { msg: 'User must have a password'},
          notEmpty: { msg: 'Password must not be empty'},
          isEmail: { msg: 'Must be a valid email address'}
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Users');
  }
};