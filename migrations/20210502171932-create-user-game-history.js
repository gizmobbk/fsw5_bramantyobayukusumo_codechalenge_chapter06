'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('UserGameHistories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_name: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      status: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      date_played: {
        allowNull: false,
        type: Sequelize.DATE,
        validate: { // validasi agar tidak kosong
          notNull: { msg: 'User must be making timestamp when playing'},
          notEmpty: { msg: 'Date must not be empty'}
        }
      },
      user_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        validate: { // validasi agar tidak kosong
          notNull: { msg: 'User must have user id'},
          notEmpty: { msg: 'User Id can t be empty'}
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('UserGameHistories');
  }
};